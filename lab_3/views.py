from django.http import response, HttpResponseRedirect
from django.shortcuts import redirect, render
from lab_1.models import Friend

from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required



# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
      # TODO Implement this
    friends = Friend.objects.all()
    response = {'friends':friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
  form = FriendForm(request.POST or None)
  if request.method == 'POST':
    if (form.is_valid() and request.method == 'POST'):
      form.save()
      return HttpResponseRedirect('/lab-3')
    


  
  response = {'form':form}
  return render(request, 'lab3_form.html',response)

