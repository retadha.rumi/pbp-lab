from django.db import models

# Create your models here.
class Note(models.Model):
    receiver = models.CharField(max_length=20)
    sender = models.CharField(max_length=20)
    title = models.CharField(max_length=40)
    message = models.TextField()

    def __str__(self):
        return self.title

