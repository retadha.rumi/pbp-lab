Retadha Rumi Indika Hidayat
2006485314
PBP E 2021 Semester Gasal

1. Apakah perbedaan antara JSON dan XML?
- JSON merupakan cara merepresentasikan objek, XML adalah bahasa markup dan menggunakan tag dalam merepresentasikan data
- JSON diimplementasikan berdasarkan JavaScript, sedangkan XML diambil dari SGML

source : https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
- HTML digunakan untuk menampilkan data, sedangkan XML untuk transfer data
- Pada HTML, syntax tag telah diatur dan terbatas. Pada XML, syntax tag dapat diatur oleh pengguna

source : https://www.upgrad.com/blog/html-vs-xml/
