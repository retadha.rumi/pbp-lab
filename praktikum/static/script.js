$(document).ready(function(){
      
    $.ajax({
      url: "/lab-2/json", 
      success: function(result){
          for(i=0; i < result.length; i++){
              var receiver = result[i].fields.receiver;
              var sender = result[i].fields.sender;
              var title = result[i].fields.title;
              var message = result[i].fields.message;

              var data ="<tr><td>"+receiver+"</td>"+
                        "<td>"+sender+"</td>"+
                        "<td>"+title+"</td>"+
                        "<td>"+message+"</td>"+
                        "<td><button class='btn btn-primary'>View</button><button class='btn btn-warning'>Edit</button><button class='btn btn-danger'>Delete</button></td></tr>";
              
              $("#table-attribute tbody").append(data);
          }
          
  }});

});